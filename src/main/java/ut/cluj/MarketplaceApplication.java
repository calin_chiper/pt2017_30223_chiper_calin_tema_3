package ut.cluj;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <h1>Warehouse manager - server-side</h1>
 * <h2>Spring Boot Application</h2>
 *
 * @author  Chiper Calin
 * @version 1.0
 * @since   2017-04-03
 *
 */
@EnableEncryptableProperties
@SpringBootApplication
public class MarketplaceApplication {

	public static void main(String[] args) {

		SpringApplication.run(MarketplaceApplication.class, args);
	}
}
