package ut.cluj.models;

/**
 * Represents a customer
 */
public class Customer  {
    private int id;
    private String firstName;
    private String lastName;
    private String email;

    /**
     * @param firstName First name
     * @param lastName  Last name
     * @param email Email
     */
    public Customer(String firstName, String lastName, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public Customer() {}

    /**
     * @return this Customer's id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id Customer ID
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return this Customer's first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName First name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return this Customer's last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName Last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return this Customer's email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email Email
     */
    public void setEmail(String email) {
        this.email = email;
    }

}


