package ut.cluj.models;

/**
 * Wrapper class for the Product
 * Used to normalize the Database schema
 */
public class Stock {
    private Product product;
    private int totalQuantity;

    /**
     *
     * @param product Product
     * @param totalQuantity Total quantity
     */
    public Stock(Product product, int totalQuantity) {
        this.product = product;
        this.totalQuantity = totalQuantity;
    }

    public Stock() {
    }

    /**
     *
     * @return the Product
     */
    public Product getProduct() {
        return product;
    }

    /**
     *
     * @param product Product
     */
    public void setProduct(Product product) {
        this.product = product;
    }

    /**
     *
     * @return the total quantity
     */
    public int getTotalQuantity() {
        return totalQuantity;
    }

    /**
     *
     * @param totalQuantity Total quantity
     */
    public void setTotalQuantity(int totalQuantity) {
        this.totalQuantity = totalQuantity;
    }
}

