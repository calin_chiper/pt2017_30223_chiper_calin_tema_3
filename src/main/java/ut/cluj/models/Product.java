package ut.cluj.models;

/**
 * Represents a product
 */
public class Product {
    private int id;
    private String productName;
    private String category;
    private String description;
    private double price;

    /**
     *
     * @param productName Product name
     * @param category Category
     * @param description Description
     * @param price Price
     */
    public Product(String productName, String category, String description, double price) {
        this.productName = productName;
        this.category = category;
        this.description = description;
        this.price = price;
    }

    public Product() {}

    /**
     *
     * @return the ID of this Product
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id Product ID
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return the name of this Product
     */
    public String getProductName() {
        return productName;
    }

    /**
     *
     * @param productName Product name
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     *
     * @return the category of this Product
     */
    public String getCategory() {
        return category;
    }

    /**
     *
     * @param category Product category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     *
     * @return the description of this Product
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description Product description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return the price of this Product
     */
    public double getPrice() {
        return price;
    }

    /**
     *
     * @param price Product cost
     */
    public void setPrice(double price) {
        this.price = price;
    }

}
