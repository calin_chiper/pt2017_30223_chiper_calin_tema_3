package ut.cluj.models;


import java.util.Date;

/**
 * Represents the order/purchase
 * Used as a contract between a customer and a bought product
 */
public class Purchase {
   private int id;
   private Customer customer;
   private Product product;
   private Date purchaseDate;
   private int quantity;

   /**
    *
    * @param customer Customer
    * @param product Product
    * @param purchaseDate Purchase Date
    * @param quantity Quantity
    */
   public Purchase(Customer customer, Product product, Date purchaseDate, int quantity) {
      this.customer = customer;
      this.product = product;
      this.purchaseDate = purchaseDate;
      this.quantity = quantity;
   }

   public Purchase() {}

   /**
    *
    * @return the Purchase ID
    */
   public int getId() {
      return id;
   }

   /**
    *
    * @param id Purchase ID
    */
   public void setId(int id) {
      this.id = id;
   }

   /**
    *
    * @return the Customer that order a Product
    */
   public Customer getCustomer() {
      return customer;
   }

   /**
    *
    * @param customer Customer
    */
   public void setCustomer(Customer customer) {
      this.customer = customer;
   }

   /**
    *
    * @return the Product that is purchased
    */
   public Product getProduct() {
      return product;
   }

   /**
    *
    * @param product Product
    */
   public void setProduct(Product product) {
      this.product = product;
   }

   /**
    *
    * @return the purchase date
    */
   public Date getPurchaseDate() {
      return purchaseDate;
   }

   /**
    *
    * @param purchaseDate Purchase date
    */
   public void setPurchaseDate(Date purchaseDate) {
      this.purchaseDate = purchaseDate;
   }

   /**
    *
    * @return the quantity that Customer purchased
    */
   public int getQuantity() {
      return quantity;
   }

   /**
    *
    * @param quantity Quantity
    */
   public void setQuantity(int quantity) {
      this.quantity = quantity;
   }
}
