package ut.cluj.business.email;

/**
 * Service that sends emails
 */
public interface EmailService {

     void sendSimpleMessage(String to, String subject, String text);

     void sendMessageWithAttachment(String to, String subject, String text, String pathToAttachment);

}
