package ut.cluj.business.bill;


import ut.cluj.models.Purchase;

/**
 *
 */
public interface BillGenerator {

    void generateBill(Purchase purchase);
}
