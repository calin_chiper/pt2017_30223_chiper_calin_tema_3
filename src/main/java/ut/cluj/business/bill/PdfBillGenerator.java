package ut.cluj.business.bill;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.TextAlignment;
import org.springframework.stereotype.Service;
import ut.cluj.models.Purchase;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * Represents a PDF bill generator
 */
public class PdfBillGenerator implements BillGenerator {
    public static final String BILL_PDF_PATH = "/resources/bills/bill.pdf";

    /**
     * Generates a PDF Bill local in /resources/bills/
     * @param purchase Purchase
     */
    public void generateBill(Purchase purchase) {
        try {
            File file = new File(BILL_PDF_PATH);
            file.getParentFile().mkdirs();
            PdfWriter writer = new PdfWriter(BILL_PDF_PATH);
            PdfDocument pdf = new PdfDocument(writer);
            Document document = new Document(pdf);

            document.add(new Paragraph("Purchase bill ").setTextAlignment(TextAlignment.CENTER));
            document.add(new Paragraph("Customer: " + purchase.getCustomer().getFirstName()
                    + purchase.getCustomer().getLastName()));
            document.add(new Paragraph("Purchase: " + purchase.getQuantity() + " x "
                    + purchase.getProduct().getProductName()));
            document.add(new Paragraph("Price: "
                    + "$" + purchase.getQuantity() * purchase.getProduct().getPrice()));
            document.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
}
