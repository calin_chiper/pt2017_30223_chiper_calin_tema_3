package ut.cluj.business.bill;

import org.springframework.stereotype.Service;

/**
 * Selects the type of the bill format
 *
 */
@Service("billGeneratorFactory")
public class BillGeneratorFactory {

    /**
     *
     * @param format File format
     * @return a bill generator
     */
    public BillGenerator getBillGenerator(BillFormat format) {
        switch (format) {
            case PDF:
                return new PdfBillGenerator();
            default:
                return null;
        }
    }
}
