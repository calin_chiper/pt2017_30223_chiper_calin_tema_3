package ut.cluj.business.exceptions;

/**
 * Thrown when the quantity from a purchase is invalid
 */
public class InvalidQuantityException extends Exception {

}
