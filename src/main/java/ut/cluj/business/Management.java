package ut.cluj.business;


import java.util.List;

/**
 * Manages objects
 * @param <T> Generic class
 */
public interface Management<T> {

    T add(T object);

    T remove(T object);

    T update(T object);

    T get(int id);

    List<T> getAll();
}
