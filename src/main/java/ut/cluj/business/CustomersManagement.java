package ut.cluj.business;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ut.cluj.models.Customer;
import ut.cluj.persistance.JdbcDAO;

import java.util.List;

/**
 * Service that manages the customers
 */
@Service(value = "customersManagement")
public class CustomersManagement implements Management<Customer> {

    private final JdbcDAO<Customer> customerDAO;

    /**
     *
     * @param customerDAO Customer data access object
     */
    @Autowired
    public CustomersManagement(JdbcDAO<Customer> customerDAO) {
        this.customerDAO = customerDAO;
    }

    /**
     * Adds a new customer
     * @param customer Customer
     * @return Customer
     */
    @Override
    public Customer add(Customer customer) {
        customerDAO.create(customer);
        return customer;
    }

    /**
     * Removes a customer
     * @param customer Customer
     * @return Customer
     */
    @Override
    public Customer remove(Customer customer) {
        customerDAO.delete(customer.getId());
        return customer;
    }

    /**
     * Updates customer details
     * @param customer Customer
     * @return the updated customer
     */
    @Override
    public Customer update(Customer customer) {
        customerDAO.update(customer);
        return customer;
    }

    /**
     * Get a customer
     * @param id ID
     * @return Customer
     */
    @Override
    public Customer get(int id) {
        return customerDAO.read(id);
    }

    /**
     * Get a list of customers
     * @return a list of customers
     */
    @Override
    public List<Customer> getAll() {
        return customerDAO.readAll();
    }
}
