package ut.cluj.business;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ut.cluj.business.bill.BillFormat;
import ut.cluj.business.bill.BillGenerator;
import ut.cluj.business.bill.BillGeneratorFactory;
import ut.cluj.business.bill.PdfBillGenerator;
import ut.cluj.business.email.EmailService;
import ut.cluj.business.exceptions.InvalidQuantityException;
import ut.cluj.models.Purchase;
import ut.cluj.models.Stock;
import ut.cluj.persistance.JdbcDAO;

import java.util.Date;
import java.util.List;

@Service
public class OrderProcessing {

    private final JdbcDAO<Purchase> purchaseDAO;

    private final JdbcDAO<Stock> stockDAO;

    private final BillGeneratorFactory billGeneratorFactory;

    private final EmailService customerEmailService;

    /**
     *
     * @param purchaseDAO Purchase data access object
     * @param stockDAO  Stock data access object
     * @param billGeneratorFactory  Bill generator Factory
     * @param customerEmailService  Customer email service
     */
    @Autowired
    public OrderProcessing(JdbcDAO<Purchase> purchaseDAO, JdbcDAO<Stock> stockDAO, BillGeneratorFactory billGeneratorFactory, EmailService customerEmailService) {
        this.purchaseDAO = purchaseDAO;
        this.stockDAO = stockDAO;
        this.billGeneratorFactory = billGeneratorFactory;
        this.customerEmailService = customerEmailService;
    }

    /**
     * Process an order/purchase
     * @param purchase Purchase
     * @return Purchase
     * @throws InvalidQuantityException if the quantity values is invalid
     */
    public Purchase createOrder(Purchase purchase) throws InvalidQuantityException {
        purchase.setPurchaseDate(new Date());
        Stock stock = stockDAO.read(purchase.getProduct().getId());

        if (purchase.getQuantity() <= 0 || stock.getTotalQuantity() - purchase.getQuantity() < 0) {
            throw new InvalidQuantityException();
        } else {
            purchaseDAO.create(purchase);
            stock.setTotalQuantity(stock.getTotalQuantity() - purchase.getQuantity());
            stockDAO.update(stock);
            sendBill(purchase, BillFormat.PDF);
        }

        return purchase;
    }

    private void sendBill(Purchase purchase, BillFormat format) {
        billGeneratorFactory.getBillGenerator(format).generateBill(purchase);
        customerEmailService.sendMessageWithAttachment(purchase.getCustomer().getEmail(),
                "Warehouse - Bill", "We sent the bill for your latest purchase",
                PdfBillGenerator.BILL_PDF_PATH);
    }

    /**
     *
     * @return a list of orders
     */
    public List<Purchase> getOrders() {
        return purchaseDAO.readAll();
    }

}
