package ut.cluj.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ut.cluj.models.Product;
import ut.cluj.models.Stock;
import ut.cluj.persistance.JdbcDAO;
import ut.cluj.persistance.ProductDAO;

import java.util.List;

/**
 * Service that manages the stock
 */
@Service(value = "stockManagement")
public class StockManagement implements Management<Stock>  {

    private final JdbcDAO<Stock> stockDAO;

    private final JdbcDAO<Product> productDAO;

    /**
     *
     * @param stockDAO Stock data access object
     * @param productDAO Student data access object
     */
    @Autowired
    public StockManagement(JdbcDAO<Stock> stockDAO, JdbcDAO<Product> productDAO) {
        this.stockDAO = stockDAO;
        this.productDAO = productDAO;
    }

    /**
     * Adds a new stock
     * @param stock Stock
     * @return Stock
     */
    @Override
    public Stock add(Stock stock) {
        productDAO.create(stock.getProduct());
        if(productDAO instanceof ProductDAO) {
            stock.setProduct(( (ProductDAO) productDAO).readByName(stock.getProduct().getProductName()));
        }
        stockDAO.create(stock);
        return stock;
    }

    /**
     * Removes a stock
     * @param stock Stock
     * @return  Stock
     */
    @Override
    public Stock remove(Stock stock) {
        stockDAO.delete(stock.getProduct().getId());
        return stock;
    }

    /**
     * Updates stock product details
     * @param stock Stock
     * @return the updated stock
     */
    @Override
    public Stock update(Stock stock) {
        productDAO.update(stock.getProduct());
        stockDAO.update(stock);
        return stock;
    }

    /**
     * Get a stock product
     * @param id ID
     * @return Stock
     */
    @Override
    public Stock get(int id) {
        return stockDAO.read(id);
    }

    /**
     * Get a stock
     * @return list of Stock objects
     */
    @Override
    public List<Stock> getAll() {
        return stockDAO.readAll();
    }
}
