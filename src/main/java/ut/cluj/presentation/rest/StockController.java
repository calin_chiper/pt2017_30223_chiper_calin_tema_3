package ut.cluj.presentation.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ut.cluj.business.Management;
import ut.cluj.models.Stock;

import java.util.List;


@RestController
@RequestMapping("/stock")
public class StockController {

    private final Management<Stock> stockManagement;

    @Autowired
    public StockController(Management<Stock> stockManagement) {
        this.stockManagement = stockManagement;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Stock> addProduct(@RequestBody Stock stock) {
        return new ResponseEntity<>(stockManagement.add(stock), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Stock>> getAllProducts() {
        return new ResponseEntity<>(stockManagement.getAll(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<Stock> updateProduct(@RequestBody Stock stock) {
        return new ResponseEntity<>(stockManagement.update(stock), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public ResponseEntity<Stock> deleteProduct(@RequestBody Stock stock) {
        return new ResponseEntity<>(stockManagement.remove(stock), HttpStatus.OK);
    }


}
