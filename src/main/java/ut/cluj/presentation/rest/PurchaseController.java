package ut.cluj.presentation.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ut.cluj.business.OrderProcessing;
import ut.cluj.business.exceptions.InvalidQuantityException;
import ut.cluj.models.Purchase;

import java.util.List;


@RestController
public class PurchaseController {

    private final OrderProcessing orderProcessing;

    @Autowired
    public PurchaseController(OrderProcessing orderProcessing) {
        this.orderProcessing = orderProcessing;
    }

    @RequestMapping(value = "/order", method = RequestMethod.POST)
    public ResponseEntity<Purchase> processOrder(@RequestBody Purchase purchase) {
        try {
            return new ResponseEntity<>(orderProcessing.createOrder(purchase), HttpStatus.OK);
        } catch (InvalidQuantityException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/history", method = RequestMethod.GET)
    public ResponseEntity<List<Purchase>> retrievePurchaseHistory() {
        return new ResponseEntity<>(orderProcessing.getOrders(), HttpStatus.OK);
    }

}
