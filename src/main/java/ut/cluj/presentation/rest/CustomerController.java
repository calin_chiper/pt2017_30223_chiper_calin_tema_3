package ut.cluj.presentation.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ut.cluj.business.Management;
import ut.cluj.models.Customer;

import java.util.List;

/**
 * Customer REST controller
 * Mapped to '/customers'
 */
@RestController
@RequestMapping("/customers")
public class CustomerController {

    private final Management<Customer> customerManagement;

    @Autowired
    public CustomerController(Management<Customer> customerManagement) {
        this.customerManagement = customerManagement;
    }


    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Customer> addCustomer(@RequestBody Customer customer) {
        return new ResponseEntity<>(customerManagement.add(customer), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<Customer> updateCustomer(@RequestBody Customer customer) {
        return new ResponseEntity<>(customerManagement.update(customer), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Customer>> getAllCustomers() {
        List<Customer> customers = customerManagement.getAll();
        return new ResponseEntity<>(customers, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public ResponseEntity<Customer> removeCustomer(@RequestBody Customer customer) {
        return new ResponseEntity<>(customerManagement.remove(customer), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id]")
    public ResponseEntity<Customer> getCustomer(@PathVariable int id) {
        return new ResponseEntity<>(customerManagement.get(id), HttpStatus.OK);
    }


}
