package ut.cluj.persistance;

import java.util.List;

/**
 * Generic data access object
 * @param <T> Generic class
 */
public interface GenericDAO<T> {

    void create(T instance);

    T read(int id);

    List<T> readAll();

    void update(T instance);

    void delete(int id);

}
