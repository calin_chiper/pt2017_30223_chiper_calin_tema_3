package ut.cluj.persistance;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ut.cluj.models.Customer;

/**
 * Customer data access object
 */
@Repository("customerDAO")
public class CustomerDAO extends JdbcDAO<Customer> {

    public CustomerDAO(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

}
