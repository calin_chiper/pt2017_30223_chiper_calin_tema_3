package ut.cluj.persistance;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ut.cluj.models.Product;

/**
 * Product data access object
 */
@Repository("productDAO")
public class ProductDAO extends JdbcDAO<Product> {
    public ProductDAO(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    /**
     *
     * @param productName Product name
     * @return a Product object from the database based on Product name
     */
    public Product readByName(String productName) {
        String query = createSelectQuery("productName");
        return getJdbcTemplate().queryForObject(query, new Object[]{productName},
                new BeanPropertyRowMapper<>(Product.class));
    }
}
