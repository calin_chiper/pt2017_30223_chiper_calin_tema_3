package ut.cluj.persistance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.validation.constraints.NotNull;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 *Represents the data access object used for JDBC C.R.U.D. methods
 *
 * @param <T> Generic class - Must be 1:1 with the SQL table
 */
public class JdbcDAO<T> implements GenericDAO<T> {

    private Class<T> genericClass;
    private final JdbcTemplate jdbcTemplate;

    /**
     *
     * @param jdbcTemplate Spring Data Jdbc Template
     */
    @Autowired
    @SuppressWarnings("unchecked")
    public JdbcDAO(JdbcTemplate jdbcTemplate) {
        ParameterizedType parameterizedType = (ParameterizedType) getClass().getGenericSuperclass();
        this.genericClass = (Class<T>) parameterizedType.getActualTypeArguments()[0];
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     *
     * @return Spring Data Jdbc Template
     */
    protected JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    /**
     *
     * @param conditionalField Conditional field
     * @return a SELECT query based on class fields
     */
    protected String createSelectQuery(@NotNull String conditionalField) {
        String query =
                "SELECT " +
                "* " +
                "FROM " +
                this.genericClass.getSimpleName() +
                " WHERE " + conditionalField + " = ?";
        return query;
    }

    private String createSelectAllQuery() {
        String query = "SELECT * FROM " + this.genericClass.getSimpleName();
        return query;
    }

    /**
     *
     * @return a INSERT query based on class fields
     */
    protected String createInsertQuery() {
        String query = "INSERT INTO " + this.genericClass.getSimpleName() +
                       " VALUES (";
        int numberOfFields = this.genericClass.getDeclaredFields().length;
        while(numberOfFields > 1) {
            query += "?, ";
            numberOfFields--;
        }
        query += "? )";
        return query;
    }

    private String createUpdateQuery(@NotNull String conditionalField) {
        String query = "UPDATE " + this.genericClass.getSimpleName() +
                       " SET ";
        Field[] fields = this.genericClass.getDeclaredFields();
        for(int cols = 1; cols < fields.length - 1; cols++) {
            query += fields[cols].getName() + " = ?, ";
        }
        query += fields[fields.length - 1].getName() + " = ? ";
        query += "WHERE " + conditionalField + " = ?";
        return query;
    }

    private String createDeleteQuery(@NotNull String conditionalField) {
        String query = "DELETE FROM " +
                        this.genericClass.getSimpleName() +
                       " WHERE " + conditionalField + " = ?";
        return query;
    }

    private boolean isGetter(Method method) {
        return method.getName().startsWith("get") &&
                method.getParameterTypes().length == 0 &&
                !void.class.equals(method.getReturnType());
    }

    /**
     * Inserts an object in the database
     * @param instance Generic instance
     */
    @Override
    public void create(T instance)  {
        String query = createInsertQuery();

        Object[] parameters = new Object[this.genericClass.getDeclaredFields().length];
        Method[] methods = this.genericClass.getMethods();
        Field[] fields = this.genericClass.getDeclaredFields();
        int index = 0;
        try {

            for (Field field : fields) {
                for (Method method : methods) {
                    if (isGetter(method) && method.getName().toUpperCase().endsWith(field.getName().toUpperCase())) {
                        parameters[index] = method.invoke(instance);  //get field value;
                        index++;
                    }
                }
            }

            jdbcTemplate.update(query, parameters);

        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param id ID
     * @return an object from the database based on id field
     */
    @Override
    public T read(int id) {
        String query = createSelectQuery("id");
        return (T) jdbcTemplate.queryForObject(query, new Object[]{id},
                new BeanPropertyRowMapper<T>(this.genericClass));
    }

    /**
     *
     * @return a list of object from the database
     */
    @Override
    public List<T> readAll() {
        String query = createSelectAllQuery();
        return jdbcTemplate.query(query, new BeanPropertyRowMapper<T>(this.genericClass));
    }

    /**
     * Updates the information of an object in database
     * @param instance Generic instance
     */
    @Override
    public void update(T instance) {
        String query = createUpdateQuery("id");
        Object[] parameters = new Object[this.genericClass.getDeclaredFields().length];
        Field[] fields = this.genericClass.getDeclaredFields();
        Method[] methods = this.genericClass.getMethods();
        int index = 0;
        try {

            for (Field field : fields) {
                for (Method method : methods) {
                    if (isGetter(method) && method.getName().toUpperCase().endsWith(field.getName().toUpperCase())) {
                        if (!field.getName().equals("id")) {
                            parameters[index] = method.invoke(instance);
                            index++;
                        }
                    }
                }
            }
            //The id must be the last parameter
            for(Method method: methods) {
                if(isGetter(method) && method.getName().endsWith("Id")) {
                    parameters[index] = method.invoke(instance);
                    break;
                }
            }

        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        jdbcTemplate.update(query, parameters);
    }

    /**
     * Deletes from database
     * @param id ID
     */
    @Override
    public void delete(int id) {
        String query = createDeleteQuery("id");
        jdbcTemplate.update(query, id);
    }
}
