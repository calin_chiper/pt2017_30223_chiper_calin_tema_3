package ut.cluj.persistance.rowmapper;


import org.springframework.jdbc.core.RowMapper;
import ut.cluj.models.Product;
import ut.cluj.models.Stock;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Maps the SQL result set into a Stock object
 * @see org.springframework.jdbc.core.RowMapper
 */
public class StockRowMapper implements RowMapper<Stock> {
    @Override
    public Stock mapRow(ResultSet resultSet, int i) throws SQLException {
        Product product = new Product();
        Stock stock = new Stock();

        /*Mapping product proprieties*/
        product.setId(resultSet.getInt("Product.id"));
        product.setProductName(resultSet.getString("Product.productName"));
        product.setPrice(resultSet.getDouble("Product.price"));
        product.setCategory(resultSet.getString("Product.category"));
        product.setDescription(resultSet.getString("Product.description"));

        /*Mapping stock proprieties*/
        stock.setTotalQuantity(resultSet.getInt("Stock.totalQuantity"));
        stock.setProduct(product);

        return stock;
    }
}
