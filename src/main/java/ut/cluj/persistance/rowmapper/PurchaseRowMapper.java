package ut.cluj.persistance.rowmapper;


import org.springframework.jdbc.core.RowMapper;
import ut.cluj.models.Customer;
import ut.cluj.models.Product;
import ut.cluj.models.Purchase;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Maps the SQL result set into a Purchase object
 * @see org.springframework.jdbc.core.RowMapper
 */
public class PurchaseRowMapper implements RowMapper<Purchase> {

    @Override
    public Purchase mapRow(ResultSet resultSet, int i) throws SQLException {
        Customer customer = new Customer();
        Product product = new Product();
        Purchase purchase = new Purchase();

        /*Mapping customer proprieties*/
        customer.setId(resultSet.getInt("Customer.id"));
        customer.setFirstName(resultSet.getString("Customer.firstName"));
        customer.setLastName(resultSet.getString("Customer.lastName"));
        customer.setEmail(resultSet.getString("Customer.email"));

        /*Mapping product proprieties*/
        product.setId(resultSet.getInt("Product.id"));
        product.setProductName(resultSet.getString("Product.productName"));
        product.setDescription(resultSet.getString("Product.description"));
        product.setCategory(resultSet.getString("Product.category"));
        product.setPrice(resultSet.getDouble("Product.price"));

        /*Mapping purchase proprieties*/
        purchase.setId(resultSet.getInt("Purchase.id"));
        purchase.setPurchaseDate(resultSet.getDate("Purchase.purchaseDate"));
        purchase.setQuantity(resultSet.getInt("Purchase.quantity"));
        purchase.setCustomer(customer);
        purchase.setProduct(product);

        return purchase;
    }
}
