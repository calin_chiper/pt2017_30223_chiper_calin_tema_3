package ut.cluj.persistance;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ut.cluj.models.Purchase;
import ut.cluj.persistance.rowmapper.PurchaseRowMapper;

import java.sql.Date;
import java.util.List;

/**
 * Purchase data access object
 */
@Repository("purchaseDAO")
public class PurchaseDAO extends JdbcDAO<Purchase> {
    public PurchaseDAO(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    /**
     * Inserts a Purchase object into databse
     * @param purchase Purchase
     */
    @Override
    public void create(Purchase purchase) {
        String query = createInsertQuery();
        Date sqlPurchaseDate = new Date(purchase.getPurchaseDate().getTime());
        Object[] parameters = new Object[] {
                purchase.getId(),
                purchase.getProduct().getId(),
                purchase.getCustomer().getId(),
                sqlPurchaseDate,
                purchase.getQuantity()
        };
        getJdbcTemplate().update(query, parameters);
    }

    /**
     *
     * @param id ID
     * @return a Purchase object based on ID
     */
    @Override
    public Purchase read(int id) {
        String joinedQuery = "SELECT * FROM Purchase, Product, Customer " +
                       " WHERE Purchase.customerId = Customer.id " +
                       " AND Purchase.productId = Product.id " +
                       " AND Purchase.id = " + id;
        return getJdbcTemplate().queryForObject(joinedQuery, new PurchaseRowMapper());
    }

    /**
     *
     * @return a list of Purchase objects
     */
    @Override
    public List<Purchase> readAll() {
        String joinedQuery = "SELECT * FROM Purchase, Product, Customer " +
                " WHERE Purchase.customerId = Customer.id " +
                " AND Purchase.productId = Product.id ";
        return getJdbcTemplate().query(joinedQuery, new PurchaseRowMapper());
    }

    /**
     * Updates an object from database
     * @param purchase Purchase
     */
    @Override
    public void update(Purchase purchase) {
        String query = "UPDATE Purchase " +
                       "SET productId = ?, customerId = ?, purchaseDate = ?, quantity = ? " +
                       "WHERE id = ?";
        Date sqlPurchaseDate = new Date(purchase.getPurchaseDate().getTime());
        Object parameters = new Object[] {
                purchase.getId(),
                purchase.getProduct().getId(),
                purchase.getCustomer().getId(),
                sqlPurchaseDate,
                purchase.getQuantity(),
        };
        getJdbcTemplate().update(query, parameters);
    }
}
