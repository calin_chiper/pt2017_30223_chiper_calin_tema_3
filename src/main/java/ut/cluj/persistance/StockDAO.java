package ut.cluj.persistance;


import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ut.cluj.models.Stock;
import ut.cluj.persistance.rowmapper.StockRowMapper;

import java.util.List;

/**
 * Stock data access object
 */
@Repository("stockDAO")
public class StockDAO extends JdbcDAO<Stock> {
    public StockDAO(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    /**
     * Inserts a Stock object into the database
     * @param stock Stock
     */
    @Override
    public void create(Stock stock) {
        String query = "INSERT INTO Stock " +
                       "VALUES(?, ?)";
        Object[] parameters = new Object[] {
                stock.getProduct().getId(),
                stock.getTotalQuantity()
        };
        getJdbcTemplate().update(query, parameters);
    }

    /**
     *
     * @param productId ID
     * @return a Stock object based on ID
     */
    @Override
    public Stock read(int productId) {
        String joinedQuery = "SELECT * " +
                "FROM Product, Stock " +
                "WHERE Stock.productId = " + productId + " GROUP BY Stock.productId";
        return getJdbcTemplate().queryForObject(joinedQuery, new StockRowMapper());
    }

    /**
     *
     * @return a list of Stock objects
     */
    @Override
    public List<Stock> readAll() {
        String joinedQuery = "SELECT * " +
                "FROM Product, Stock " +
                "WHERE Stock.productId = Product.id ";
        return getJdbcTemplate().query(joinedQuery, new StockRowMapper());
    }

    /**
     * Updates a Stock object from database
     * @param stock Stock
     */
    @Override
    public void update(Stock stock) {
        String query = "UPDATE Stock " +
                "SET totalQuantity = ? " +
                "WHERE productId = ?";
        Object[] parameters = new Object[] {
                stock.getTotalQuantity(),
                stock.getProduct().getId()
        };
        getJdbcTemplate().update(query, parameters);
    }

    /**
     * Deletes a Stock object from database
     * @param id ID
     */
    @Override
    public void delete(int id) {
        String query = "DELETE FROM Product WHERE id = ?";
        getJdbcTemplate().update(query, id);
    }
}
